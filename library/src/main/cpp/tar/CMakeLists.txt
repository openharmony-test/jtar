cmake_minimum_required(VERSION 3.4.1)


set(can_use_assembler TRUE)
enable_language(ASM)
IF("${OHOS_ARCH}" STREQUAL "arm64-v8a")
 SET(ASM_OPTIONS "-x assembler-with-cpp")
 SET(CMAKE_ASM_FLAGS "${CFLAGS} ${ASM_OPTIONS} -march=armv8+crypto -D__OHOS__")
ENDIF()

project(ctar)

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

set(TAGET_TAR_SRC_PATH ${CMAKE_CURRENT_SOURCE_DIR}/tar)

add_library(ctar

        # Sets the library as a shared library.
        SHARED

        # Provides a relative path to your source file(s).
        ${TAGET_TAR_SRC_PATH}/tar.cpp
        )

target_link_libraries(ctar PUBLIC libace_napi.z.so libhilog_ndk.z.so -s libc++.a)
target_include_directories(ctar PUBLIC ${NATIVERENDER_ROOT_PATH}
                                       ${NATIVERENDER_ROOT_PATH}/tar/tar
                                       )


